 Command line instructions
Git global setup

git config --global user.name "Md. Sajib Hosen"
git config --global user.email "0sajib0@gmail.com"

Create a new repository

git clone https://gitlab.com/0sajib0/test.git
cd test
touch README.md
git add README.md
git commit -m "add README"
git push -u origin master

Existing folder

cd existing_folder
git init
git remote add origin https://gitlab.com/0sajib0/test.git
git add .
git commit -m "Initial commit"
git push -u origin master

Existing Git repository

cd existing_repo
git remote rename origin old-origin
git remote add origin https://gitlab.com/0sajib0/test.git
git push -u origin --all
git push -u origin --tags