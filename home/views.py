from django.shortcuts import render,HttpResponse
import datetime

# Create your views here.
def home2(request,number):
   text = "<h1>welcome to my app number %s!</h1>"% number
   return HttpResponse(text)
def home(request):
   # return HttpResponse('<h1>Hello i am from home views</h1>')
   today = datetime.datetime.now().date()
   daysOfWeek = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']
   return render(request, "index.html", {"today" : today, "days_of_week" : daysOfWeek})
