from django.urls import path
from contact.views import contact,contactwithouturl,save,style
#from contact.views import contactwithouturl
#from contact.views import save

#urlpatterns = patterns('', url(r'^hello/', 'contact.views.contact', name = 'contact'),)
urlpatterns = [
	path('test/<int:num>', contact, name='contact'),
	path('test/', contactwithouturl, name='contactwithouturl'),
    path('save/', save, name='save'),
	path('style/', style, name='style'),
]
